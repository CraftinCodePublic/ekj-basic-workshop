package zmienne;

public class ZmienneIntro {
    public static void main(String[] args) { // psvm + tab
        System.out.println(); // sout + tab
        int a = 10;
        int b = 20;
        int c = 30;

        System.out.println("Zmienna a ma wartosc:" + a);
        System.out.println("Zmienna b ma wartosc:" + b);
        System.out.println("Zmienna c ma wartosc:" + c);

        a=100;
        b=200;
        c=300;

        System.out.println("Zmienna a ma wartosc:" + a);
        System.out.println("Zmienna b ma wartosc:" + b);
        System.out.println("Zmienna c ma wartosc:" + c);

        long z = 3000000000L;

        float jakisFloat = 21.5F;
        double jakisDouble = 21.5;

        int ilorazInt = 2/3;
        System.out.println("iloraz int:" + ilorazInt);
        double iloraz = 10/3;
        System.out.println(iloraz);
        iloraz = 10/3d;
        System.out.println(iloraz);
    }
}

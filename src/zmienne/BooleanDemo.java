package zmienne;

public class BooleanDemo {
    public static void main(String[] args) {
        boolean maKomorke = false;
        boolean maInternet = false;
        boolean maStacjonarny = true;

        boolean dotyczyGoPromocja =
                maKomorke && maInternet || maStacjonarny;
        System.out.println(dotyczyGoPromocja);
    }
}

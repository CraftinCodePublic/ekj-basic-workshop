package instrukcja_if;

public class IfDemo {
    public static void main(String[] args) {
        int wiek = 28;
        if (wiek >= 18) {
            System.out.println("Może wziąć kredyt");
            if (wiek <= 26) {
                System.out.println("\tale tylko mały");
            } else {
                System.out.println("\ti to nawet duży");
            }
        } else {
//        if(!(wiek >=18) ){
            System.out.println("Nie może");
        }


        int x = 1;

        if (x == 1) {
            System.out.println("X jest równe jeden");
        } else if (x == 2) {
            System.out.println("X jest równe dwa");
        } else if (x == 3) {
            System.out.println("X jest równe trzy");
        }


        int max = 10;
        int punkty = 2;
        // <0%;30%) -> 1, <30%;50%) -> 2, <50%;100%) -> 3
        double procent = punkty / (double) max; // operator rzutowania
        // przy uzyciu ifow
        if (procent < 0.3) {
            System.out.println("jedynka");
        }
        if (procent >= 0.3 && procent < 0.5) {
            System.out.println("dwójka");
        }
        if (procent >= 0.5) {
            System.out.println("trójka");
        }

        // przy uzyciu else ifow
        if (procent < 0.3) {
            System.out.println("jedynka");
        } else if (procent < 0.5) {
            System.out.println("dwójka");
        } else if (procent >= 0.5) {
            System.out.println("trójka");
        }

        // przy uzyciu else ifow i elsa
        if (procent < 0.3) {
            System.out.println("jedynka");
        } else if (procent >= 0.5) {
            System.out.println("trójka");
        } else  {
            System.out.println("dwójkę");
        }


    }
}

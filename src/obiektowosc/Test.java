package obiektowosc;

public class Test {
    public static void main(String[] args) {
        Klient adek = new Klient(123, "Adrian");
        Klient janek = new Klient(321, "Jan");

        adek.przedstawSie();
        adek.zablokuj();

        if(!adek.zablokowany){
            System.out.println("Co chcesz wypozyczyc?");
        }

    }
}

package obiektowosc;

public class Klient {
    int numerKarty;
    String imie;
    boolean zablokowany;

    public Klient(int numerKarty, String imie) {
        this.numerKarty = numerKarty;
        this.imie = imie;
    }

    public void przedstawSie(){
        System.out.println("jestem "+ imie);
    }

    public void zablokuj(){
        zablokowany = true;
    }
}
